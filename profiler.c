#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*
  This is the source code for profiler.program is broken into two functions- 
  
  a. main - reads the file and goes through each line. makes required changes according to the followed condition.
  b.check - two parameters are passed to this function. first is the line read from the source code second is the keyword we are looking for.
  It returns true if the keyword found in that line.
  
  Below step are description of how it executes-
  
  1. In the main function it reads the file.
  2. Goes through each line of file in the while loop.
  3. first checks for #include<iostream> in C++ file and if it is present add the statements declaring the variables required for the process.
  4. In each iteration it checks for open brackets and if it is present then adds the array increment operator for that.
  5. Also in each iteration looks for return keyword if true then writed the result in a file.
  
*/

//This function chops out the pices if substring. start index and the length of substring is given as the parameter.
/*
1. declares a temporary array of the size provided in the parameter.
2. iterates through char array and copy the data from those index.
3. enters null terminator in the end.
*/

//char *getToken(char buffer[],int start,int len)
//{
//	char token[(len+1)];
//    int i=0;int index=0;
//    for(i=start;i<(start+len);i++)
//    {
//      token[index] = buffer[i];  
//      index++;
//    }
//    
//    token[index] = 0;
//    return token;
//}

//This function checks if the given string data is present in the passed line (buffer)
/*
1. Takes the line of source code(buffer) and keyword as parameter.
2. checks the length of keyword. 
3. iterated through each index of buffer and takes the substring out of the length equal the keyword.
4. If the substring and keyword are same for any iteration than it's true.
*/
int checkToken(char buffer[],char data[])
{
	int i,index=0;
	printf ("Passed buffer %s",buffer);
	//printf(" strlen %d",strlen(data) );
	index = strlen(data);
	//while(data[index]!=0)
	//	{
	//		index++;
	//	}
	
	for(i=0;i<1024;i++)
	{
		if(buffer[i]!=0 && buffer[i]!=32)
		{
			//substring is copied into temp char[]
			char temp[index];
			memcpy(temp,buffer+i,index);
			if((strcmp(temp,data))==0)
			{
			//	printf("\ntemp is %d and data is %d and buffer is %d for index %d",temp[0],data[0],buffer[i],i);
			//	printf("string comparison %d ",(strcmp(temp,data)));
				return 1;
			}
			if(buffer[i]==0)
			{
				return 0;
			}
			temp[0] = 0;
		}
	}
	return 0;

}

//looks for the open bracket in the file.

int getBracket(char buffer[])
{
	int i;
	for(i=0;i<1024;i++)
	{
		if(buffer[i]==0)
			break;
			
		if(buffer[i]==123)
		{
			return 1;		
		}
	}
	return 0;
}


//description for this function is given on top

int main()
{
	FILE *fp = fopen("main.cpp","r");
	FILE *fp1 = fopen("main1.cpp","w");
	/*
	1.buffer holds the line of source code.
	2.result is the string to be appended in the end. It is the source code responsible for writing output file.
	With each array incrementer added to the file it also appended to the result string.
	3.cmd holds the int array whihc keeps the count of each block. 
	*/
	char buffer[1024];char temp[1024];
	char result[1024] = "\nofstream myfile;  \nmyfile.open (\"example.txt\"); \nmyfile";
	char cmd[30] = "\nF[0";
	/*
	1. when count id 0 we add the initial variable and function required for calculation.
	2. index is to keep the track of main array used to keep the count.
	3. if main is 1 than main function found in the next line after the "{" call the init function which initilize the main array.
	*/
	int count=0,index=0,i,main=0,loop=0;
	
	
 	//main loop
	while(fgets(buffer,1024,fp))
	{
		
		if(!count && checkToken(buffer,"#include <iostream>"))
		{
			strcat( buffer,"\n #include <fstream> \nint F[100];void init(){ for(int i=0;i<100;i++) F[i]=0;}");
			count++;
			fprintf(fp1,buffer);
			for(i=0;i<1024;i++)
		    {
				buffer[i]=0;
			}
		}
		
		if(checkToken(buffer,"main"))
		{
			main++;
		}
		
		if(checkToken(buffer,"for") || checkToken(buffer,"while") || checkToken(buffer,"if"))
		{
			printf("for loop in this line");
			loop++;
		}
	
		
		//checkTokens if there is an opening brace
		//printf("buffer %s ",buffer);
		if(getBracket(buffer))
		{
			if(main==1)
			{
				strcat( buffer,"\ninit();");
				main++;
			}
			if(loop==1 || loop==2)
			{
				loop=0;
			}
			if(index<10)
			{
				cmd[3]=index+48;
				cmd[4]=']';
				cmd[5]=0;
			}
			else if(index>9 && index<100)
			{
				cmd[4] = (index%10);
				int i= index/10;
				cmd[3] = i;
				cmd[5]=']';
				cmd[6]=0;
			}
			
			printf("cmd is %s \n",cmd);
			strcat( buffer,cmd);
			strcat(buffer,"++;");
			strcat(result,"<<\"\t\"<<");
			strcat(result,cmd);
			
			index++;
			
		}
		else if(loop==2)
		{
			if(index<10)
			{
				cmd[3]=index+48;
				cmd[4]=']';
				cmd[5]=0;
			}
			else if(index>9 && index<100)
			{
				cmd[4] = (index%10);
				int i= index/10;
				cmd[3] = i;
				cmd[5]=']';
				cmd[6]=0;
			}
			strcpy(temp,"{");
			cmd[0]=32;
			strcat(temp,cmd);
			strcat(temp,"++;");
			strcat(temp,buffer);
			strcat(temp,"}");
			strcpy(buffer,temp);
			printf("cmd is %s \n",cmd);
			strcat(result,"<<\"\t\"<<");
			strcat(result,cmd);
			
			index++;
			loop=0;	
		}
		
		if(loop==1)
		{
			loop++;
		}
		if(checkToken(buffer,"return"))
		{
			printf("return in the buffer");
			strcat(result,";\nmyfile.close();");
			strcat(result,buffer);
			strcpy(buffer,result);
			printf("%s",buffer);
			fprintf(fp1,buffer);
			for(i=0;i<1024;i++)
		    {
				buffer[i]=0;
			}
		}
		else
		{
			fprintf(fp1,buffer);
		}
	//	printf("\n %s",buffer);
	
		
	}
	
	fclose(fp);
	fclose(fp1);
	//printf("%d",1);

	return 0;
}
